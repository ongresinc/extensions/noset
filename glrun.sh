#!/bin/bash
# ./glrun.sh pg12

# set -ex

V=${1:-pg14}

init () {
	docker stop gitlab-runner-noset 2> /dev/null 

	docker rm gitlab-runner-noset 2> /dev/null

	docker run -d \
		--name gitlab-runner-noset \
		-v $PWD:$PWD \
		-v $PWD/log:/var/log/postgresql/ \
		-v $PWD/log/initdb.log:/builds/project-0/log/initdb.log \
		-v /var/run/docker.sock:/var/run/docker.sock \
		gitlab/gitlab-runner:latest

}

[[ "${V}"=="init" ]] && init || docker exec -it -w $PWD gitlab-runner-noset gitlab-runner exec docker ${V}