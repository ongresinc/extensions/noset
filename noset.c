/*-------------------------------------------------------------------------
 * noset.c
 *
 *  This extension allows to configure non-superuser privileges for   
 * 	setting Postgres variables.
 * 
 * Portions Copyright (c) 2021 - , OnGres Inc.
 * 
 * Maintainers:
 * 
 * 	Anthony Sotolongo <asotolongo@ongres.com>
 * 	Emanuel Calvo <emanuel@ongres.com>
 * 	Sebastian Webber <swebber@ongres.com>
 * 
 *-------------------------------------------------------------------------
 */

#include "postgres.h"
#include "miscadmin.h"
#include "tcop/utility.h"
#include "utils/varlena.h"
#include "executor/spi.h"

#ifdef PG_MODULE_MAGIC
PG_MODULE_MAGIC;
#endif

#define BUFSIZE 120

void		_PG_init(void);
void		_PG_fini(void);

/*   store previously assigned hook pointer in a global variable. */
static ProcessUtility_hook_type original_processUtility_hook = NULL;

/*
 * GUC variable for noset.enabled
 * variable to disable user to execute SET command in the session
 */
static bool nosetenabled = false;
static char *nosetparameters;

/*  noset hook implementation. */
static void
disable_set(PlannedStmt *pstmt,
			const char *queryString,
#if PG_VERSION_NUM >= 140000	/* version 14 */
			bool readOnlyTree,
#endif
			ProcessUtilityContext context,
			ParamListInfo params,
			QueryEnvironment *queryEnv,
			DestReceiver *dest,
#if PG_VERSION_NUM < 130000		/* version 12 */
			char *completionTag
#else							/* version 13 + */
			QueryCompletion *qc
#endif
)

{

	Node	   *parsetree = pstmt->utilityStmt;
	VariableSetStmt *setstm = (VariableSetStmt *) parsetree;
	AlterRoleSetStmt *alterstm = (AlterRoleSetStmt *) parsetree;
	char	   *parameters_text;
	List	   *parameters;
	ListCell   *lc;
	char	   *username = GetUserNameFromId(GetUserId(), false);


	char		revk[BUFSIZE] = "SELECT noset_rvk('";
	char		grant[BUFSIZE] = "SELECT noset_grt('";
	int			result;

	/*
	 * Do the custom process to avoid SET/RESET command for specific role
	 * without superuser grants
	 */
	switch (nodeTag(parsetree))
	{
		/* SET */
		case T_VariableSetStmt:

			elog(DEBUG1, "noset extension (T_VariableSetStmt) invoked with user: %s, noset.enabled: %d, superuser: %d, query: %s ", username, nosetenabled, superuser(), queryString);

			/* if noset.enabled is true for this user and not superuser */
			if (nosetenabled && !superuser())
			{
				/* analyze the kind of T_VariableSetStmt */
				switch (setstm->kind)
				{
					case VAR_SET_VALUE:
					case VAR_SET_CURRENT:
					case VAR_SET_DEFAULT:
					case VAR_RESET:
						elog(DEBUG1, "noset extension T_VariableSetStmt name: %s , kind: %d", setstm->name, setstm->kind);
						elog(DEBUG1, "noset extension nosetparameters: %s ", nosetparameters);

						/* checks nosetparameters values list */
						if (strcmp(nosetparameters, "*") != 0)
						{
							parameters_text = pstrdup(nosetparameters);
							if (!SplitIdentifierString(parameters_text, ',', &parameters))
							{
								/* syntax error in parameters list */
								ereport(ERROR,
										(errcode(ERRCODE_INVALID_PARAMETER_VALUE),
										 errmsg("parameters list syntax is invalid: %s", parameters_text)));
							}

							foreach(lc, parameters)
							{
								char	   *tok = (char *) lfirst(lc);

								/*
								 * if the parameter matches one of the list of
								 * paramaters blocked
								 */
								if (strcmp(setstm->name, tok) == 0)
								{
									elog(DEBUG1, "noset extension nosetparameters token match: %s ", tok);
									ereport(ERROR,
											(errcode(ERRCODE_INSUFFICIENT_PRIVILEGE),
											 (errmsg("permission denied to set/reset parameter '%s'", queryString))));
								}
							}
							pfree(parameters_text);
							list_free(parameters);
						}

						/*
						 * if nosetparameters is equal "*", it means all
						 * paramates are blocked
						 */
						else
							ereport(ERROR,
									(errcode(ERRCODE_INSUFFICIENT_PRIVILEGE),
									 (errmsg("permission denied to set/reset parameter '%s'", queryString))));

						break;
					default:
						ereport(ERROR,
								(errcode(ERRCODE_INSUFFICIENT_PRIVILEGE),
								 (errmsg("permission denied to execute this command '%s'", queryString))));
				}

				/* break; */
			}
			break;
		/* END CASE SET */

		/* ALTER ROLE */
		case T_AlterRoleSetStmt:
			elog(DEBUG1, "noset extension (T_AlterRoleSetStmt) invoked with user: %s, to user: %s, var: %s, val: %s, query: %s ", username, alterstm->role->rolename, alterstm->setstmt->name, ExtractSetVariableArgs(alterstm->setstmt), queryString);

			switch (alterstm->setstmt->kind)
			{
				case VAR_SET_VALUE:
				case VAR_SET_CURRENT:
				case VAR_SET_DEFAULT:
				case VAR_RESET:
					elog(DEBUG1, "noset extension T_AlterRoleSetStmt username: %s , kind: %d, parametername: %s,parametervalue: %s ", alterstm->role->rolename, alterstm->setstmt->kind, alterstm->setstmt->name, ExtractSetVariableArgs(alterstm->setstmt));

					/* Alter role statement connect */
					result = SPI_connect();
					if (result < 0)
					{
						elog(ERROR, "noset extension T_AlterRoleSetStmt: SPI_connect returned %d", result);
					}

					/* true/1/on */
					if (strcmp(alterstm->setstmt->name, "noset.enabled") == 0 && (strcmp(ExtractSetVariableArgs(alterstm->setstmt), "true") == 0 || strcmp(ExtractSetVariableArgs(alterstm->setstmt), "1") == 0 || strcmp(ExtractSetVariableArgs(alterstm->setstmt), "on") == 0))
					{
						elog(DEBUG1, "noset extension T_AlterRoleSetStmt match: noset.enabled with true/1/on");

						strcat(revk, alterstm->role->rolename);
						strcat(revk, "')");
						elog(DEBUG1, "noset extension T_AlterRoleSetStmt: revk: %s", revk);
						result = SPI_execute(revk, false, 0);
						if (result != SPI_OK_SELECT)
						{
							elog(ERROR, "noset extension T_AlterRoleSetStmt: SPI_execute on revoke returned %d", result);
						}

					}

					/* false/0/off */
					if (strcmp(alterstm->setstmt->name, "noset.enabled") == 0 && (strcmp(ExtractSetVariableArgs(alterstm->setstmt), "false") == 0 || strcmp(ExtractSetVariableArgs(alterstm->setstmt), "0") == 0 || strcmp(ExtractSetVariableArgs(alterstm->setstmt), "off") == 0))
					{
						elog(DEBUG1, "noset extension T_AlterRoleSetStmt match: noset.enabled with false/0/off");

						strcat(grant, alterstm->role->rolename);
						strcat(grant, "')");
						elog(DEBUG1, "noset extension T_AlterRoleSetStmt: grant: %s", grant);
						result = SPI_execute(grant, false, 0);
						if (result != SPI_OK_SELECT)
						{
							elog(ERROR, "noset extension T_AlterRoleSetStmt: SPI_execute on grant returned %d", result);
						}

					}

					/* Alter role statement finish */
					SPI_finish();
					break;

				default:
					elog(DEBUG1, "noset extension T_AlterRoleSetStmt default:");
					break;
			}
			break;
		/* END CASE ALTER ROLE */


		default:
			break;
	}

	if (original_processUtility_hook)
		(*original_processUtility_hook) (pstmt, queryString,
#if PG_VERSION_NUM >= 140000	/* version 14 */
										 readOnlyTree,
#endif
										 context, params, queryEnv,
										 dest,
#if PG_VERSION_NUM < 130000		/* version 12 */
										 completionTag
#else							/* version 13 + */
										 qc
#endif
			);
	else
		standard_ProcessUtility(pstmt, queryString,
#if PG_VERSION_NUM >= 140000	/* version 14 */
								readOnlyTree,
#endif
								context, params, queryEnv,
								dest,
#if PG_VERSION_NUM < 130000		/* version 12 */
								completionTag
#else							/* version 13 + */
								qc
#endif
			);
}

/*  Called upon extension load. */
void
_PG_init(void)
{
	/* noset.enabled */
	DefineCustomBoolVariable("noset.enabled",
							 "noset flag to block SET/RESET commands",
							 NULL,
							 &nosetenabled,
							 false,
							 PGC_SUSET,
							 0,
							 NULL,
							 NULL,
							 NULL);

	/* noset.parameters */
	DefineCustomStringVariable("noset.parameters",
							   "noset parameters list, comma-separated list parameters for block via SET/RESET",
							   NULL,
							   &nosetparameters,
							   "*",
							   PGC_SUSET,
							   0,
							   NULL,
							   NULL,
							   NULL);

	/* Save the original hook value  for non -superuser */
	original_processUtility_hook = ProcessUtility_hook;
	/* Register noset handler. */
	ProcessUtility_hook = disable_set;
}

/*  Called with extension unload. */
void
_PG_fini(void)
{

	/* Return back the original hook value. */
	ProcessUtility_hook = original_processUtility_hook;
}
