CREATE OR REPLACE FUNCTION noset_rvk (p_user text)
    RETURNS void
    AS $$
DECLARE
    u record;
    rvk_cmd text;
BEGIN
    REVOKE EXECUTE ON FUNCTION set_config FROM public;
    raise INFO 'EXECUTE WAS REVOKED FROM PUBLIC ROLE IN FUNCTION set_config ';


    FOR u IN
    SELECT
        *
    FROM
        pg_user
    WHERE
        NOT usesuper
        AND usename NOT IN (
            SELECT
                usename
            FROM
                pg_user
            WHERE
                NOT usesuper
                AND (ARRAY['noset.enabled=on'] <@ useconfig
                    OR ARRAY['noset.enabled=true'] <@ useconfig
                    OR ARRAY['noset.enabled=1'] <@ useconfig))
            LOOP
                EXECUTE 'GRANT EXECUTE ON FUNCTION set_config TO '|| u.usename;
            END LOOP;

    rvk_cmd := 'REVOKE EXECUTE ON FUNCTION set_config FROM '||p_user;
    EXECUTE rvk_cmd;
    RETURN;
END;
$$
LANGUAGE PLPGSQL;

CREATE OR REPLACE FUNCTION noset_grt (p_user text)
    RETURNS void
    AS $$
DECLARE
    u record;
    grt_cmd text;
BEGIN
    grt_cmd := 'GRANT EXECUTE ON FUNCTION set_config TO '||p_user;
    EXECUTE grt_cmd;
    RETURN;
END;
$$
LANGUAGE PLPGSQL;
