EXTENSION = noset
MODULES = noset
OBJS = noset.so
EXTENSION = noset
DATA = noset--0.3.0.sql

TESTS 	= $(wildcard sql/*.sql)
REGRESS = $(patsubst sql/%.sql,%,$(TESTS))
REGRESS_OPTS =--temp-config=./noset.conf --temp-instance=/tmp/tmp_check --inputdir=./ --outputdir=/tmp
# REGRESS = noset


PG_CONFIG = pg_config
PGXS := $(shell $(PG_CONFIG) --pgxs)
include $(PGXS)

