 # noset module

`noset` is a loadable module via `shared_preload_libraries`,  to avoid `SET/RESET` command for change a parameter in session. It is possible to filter one or a group a parameters to block.


## Install
 
> *Required PG 12, 13 and 14(tested)* and devel postgresql's packages

```bash
make
make install
```

You must make sure you can see the binary `pg_config` in your PATH, export PG_CONFIG path, or passing it as an argument:


```bash
export PG_CONFIG=/path_to_pg_config/
make
sudo make install
```

```bash
make PG_CONFIG=/path_to_pg_config/
sudo make install PG_CONFIG=/path_to_pg_config/
```


## Settings

The `noset` module must be loaded in `shared_preload_libraries` and the service should be restarted

```
shared_preload_libraries = 'noset'
```

Create the extension

```sql
CREATE EXTENSION noset;
```

Settings can be specified to specifically user (this option not apply to a superuser), using the command:


```sql
ALTER ROLE/USER ... SET noset.enabled = true;
ALTER ROLE/USER ... SET noset.paramaters = 'name_of_blocked_parameter'; --to block a specific parameter only
 ```



**noset.enabled**: Specifies if the role / user is prohibited to run `SET/RESET` command change a parameter in his session (default false)

**noset.parameters**: Specifies if the role / user is prohibited to run `SET/RESET` for specific parameters only, can be use a comma-separated list parameters for block via SET/RESET" (default *, mean all parameters), requires  `noset.enabled = on`


## Example

```sql
psql -d db -U postgres
--creating user and setting the `noset.enabled` option
db=# create user appuser password 'mypass';
CREATE ROLE
db=# alter user appuser set noset.enabled = true;
ALTER ROLE
db=# alter user appuser set noset.parameters = 'work_mem,jit'; --block 'work_mem,jit' parameters only
ALTER ROLE
```

```sql
psql -d db -U appuser
--login as appuser
db=> select current_user;
 current_user
--------------
 appuser
(1 row)

db=> show noset.enabled ;
 noset.enabled
---------------
 on
(1 row)

db=> set work_mem ='1GB'; --failed
ERROR:  permission denied to set/reset parameter 'set work_mem = '1GB';'
db=> set maintenance_work_mem ='1GB';  --succeed
SET
db=>
```

To see the users who do not have permission to change parameters via `SET/RESET` command , you can use the following query, you can use the filters 'noset.enabled=true' or 'noset.enabled=1' too:

```
db=# select usename, useconfig from pg_user where useconfig is not null and array['noset.enabled=on']  <@ useconfig;
 usename |     useconfig
---------+--------------------
 appuser | {noset.enabled=on}
(1 row)

```

## Regression Tests

```
make installcheck
or
make installcheck PG_CONFIG=/path_to_pg_config/
```

If you want to avoid setting the whole thing, you can test using docker by:

```
docker run -it --rm --mount "type=bind,src=$(pwd),dst=/repo" pgxn/pgxn-tools bash
# cd repo
# pg-start <version>
# make install
# su postgres
# make installcheck
```


## Notes

noset extension revokes the access to the `set_config` function from the `PUBLIC` role. Note that future created non-superusers will most likely require `GRANT` for this feature.


## License

Permission to use, copy, modify, and distribute this software and its documentation for any purpose, without fee, and without a written agreement is hereby granted, provided that the above copyright notice and this paragraph and the following two paragraphs appear in all copies.

IN NO EVENT SHALL THE AUTHORS BE LIABLE TO ANY PARTY FOR DIRECT, INDIRECT, SPECIAL, INCIDENTAL, OR CONSEQUENTIAL DAMAGES, INCLUDING LOST PROFITS, ARISING OUT OF THE USE OF THIS SOFTWARE AND ITS DOCUMENTATION, EVEN IF THE AUTHORS HAVE BEEN ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.

THE AUTHORS SPECIFICALLY DISCLAIM ANY WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE. THE SOFTWARE PROVIDED HEREUNDER IS ON AN "AS IS" BASIS, AND THE AUTHORS HAVE NO OBLIGATIONS TO PROVIDE MAINTENANCE, SUPPORT, UPDATES, ENHANCEMENTS, OR MODIFICATIONS.

## Authors

2021- , `noset`, OnGres Inc.  

This module is an open project. Feel free to join us and improve this module. To find out how you can get involved, please contact us or write us: 

- Anthony Sotolongo <asotolongo@ongres.com>
- Emanuel Calvo <emanuel@ongres.com>
