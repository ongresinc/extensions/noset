\set ECHO none
CREATE EXTENSION noset;
create user appuser ;
create user appuser2 ;
alter user appuser set noset.enabled = true;
alter user appuser set noset.parameters = 'work_mem,jit';
grant pg_read_all_settings to appuser,appuser2;
show shared_preload_libraries;

\connect - appuser
select current_user;
show shared_preload_libraries;
show noset.enabled ;
set work_mem ='1GB'; --failed
show work_mem;
select set_config('work_mem','2GB',false); --failed
show work_mem;
UPDATE pg_settings SET setting = '1024' WHERE name = 'work_mem'; --failed
show work_mem;
set jit = off; --failed
show jit;
set maintenance_work_mem ='1GB'; --succeed
show maintenance_work_mem;

\connect - appuser2
select current_user;
show shared_preload_libraries;
show noset.enabled ;
set work_mem ='1GB'; --succeed
show work_mem;
select set_config('work_mem','2GB',false); ----succeed
show work_mem;
